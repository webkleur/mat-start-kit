#Future Expert Front - End Developer GUIDE
##Tools you need/need to know

- IDE : example: PhPstorm from jetbrains: https://www.jetbrains.com/
- install nodejs : https://nodejs.org/en/

- compile scss/sass to css with your IDE file watcher
- do NOT write your css in main.css but compile it in .scss with your IDE file watcher
- push your code to gitlab if done
- make sure to run: npm install

#File watcher settings in PhPstorm

- /node_modules/node-sass/bin/node-sass
- $FileName$ $FileNameWithoutExtension$.css
- $FileNameWithoutExtension$.css:$FileNameWithoutExtension$.css.map

#Guides:
- sass: https://www.youtube.com/watch?v=St5B7hnMLjg
- file watchters in Phpstorm: https://www.youtube.com/watch?v=fe14-21NTGU
- nodejs: https://www.youtube.com/watch?v=pU9Q6oiQNd0
- git: https://www.youtube.com/watch?v=HVsySz-h9r4
- bootstrap: https://www.youtube.com/watch?v=gqOEoUR5RHg
- sass vs css: https://www.youtube.com/watch?v=G_Dolh3BL6g

